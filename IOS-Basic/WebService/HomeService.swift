//
//  HomeService.swift
//  IOS-Basic
//
//  Created by nikhil patil on 05/05/21.
//

import Foundation

class HomeService :  NSObject {
    
    /*
     Use:  To fetch list
     - Parameter page: page number of listing
     - Parameter perPage: number of contacts per page
     */
    func getAstrologerList(perPage: String, page: String, completion : @escaping (Users) -> ()){
        var parameters = [String: Any]()
        parameters = ["perPage": "10", "page" : page ] as [String : Any]
        ApiHandler.shared.callSomeAPI(parameters, callUrl: Config.baseUrl + Config.getAstrologerList) { (result, wasRequestFetched, errorMessage) in
            
            print("getAstrologerList ---", result)
            if let result = result, wasRequestFetched {
                if !result["error"].boolValue, let astrologerListingModel = ApiHandler.decode(Users.self, from: result){
                    
             //       self.delegate?.didReceiveAstrologerList(error: false, message: Config.defaultErrorMessage, astrologerModel: astrologerListingModel)
                        completion(astrologerListingModel)
                } else{
                    
                }
            } else{
                
            }
        }
    }
}

