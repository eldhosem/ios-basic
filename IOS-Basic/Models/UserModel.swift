//
//  UserModel.swift
//  IOS-Basic
//
//  Created by nikhil patil on 29/04/21.
//

import Foundation
import SwiftyJSON

// MARK: - Welcome
struct Users: Decodable {
    let error : Bool?
    let title : String?
    let data: [UserModel]
}

// MARK: - UserModel
struct UserModel: Decodable {
    
    
    var firstName : String?
    var approvalStatus : String?
    var mobile : String?
    var lastName : String?
    var id : String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case firstName = "first_name"
        case approvalStatus = "approval_status"
        case mobile = "mobile"
        case lastName = "last_name"
    }
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
        init(fromJson json: JSON!){
            if json.isEmpty{
                return
            }

            firstName = json["first_name"].stringValue
            approvalStatus = json["approval_status"].stringValue
            mobile = json["mobile"].stringValue
            lastName = json["last_name"].stringValue
            id = json["_id"].stringValue
        }
}
