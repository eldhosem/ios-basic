//
//  HomeTableCell.swift
//  IOS-Basic
//
//  Created by nikhil patil on 12/05/21.
//

import UIKit

class HomeTableCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var DescLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(userData: UserModel) {
        titleLabel.text = userData.firstName
    }
}
