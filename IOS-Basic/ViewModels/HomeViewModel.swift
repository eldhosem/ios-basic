//
//  HomeViewModel.swift
//  IOS-Basic
//
//  Created by nikhil patil on 05/05/21.
//

import Foundation

class HomeViewModel : NSObject {

    private var homeService : HomeService!
    
    private(set) var userData : Users! {
        didSet {
            self.bindEmployeeViewModelToController()
        }
    }
    
    var bindEmployeeViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.homeService =  HomeService()
        callFuncToGetEmpData()
    }
    
    func callFuncToGetEmpData() {
        self.homeService.getAstrologerList(perPage: "10", page: "1", completion: { (userData) in
            self.userData = userData
        })
    }
}
