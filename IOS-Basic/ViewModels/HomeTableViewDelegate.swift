//
//  HomeTableViewDelegate.swift
//  IOS-Basic
//
//  Created by nikhil patil on 12/05/21.
//

import Foundation
import UIKit

class HomeTableViewDelegate<CELL : UITableViewCell,T> : NSObject, UITableViewDelegate {
 
    private var items : [T]!
    
    var didSelectRowAt : ((IndexPath) -> ()) = {_ in}

    var requestPage = 1
    
    init(tableView: UITableView, items : [T]) {
        super.init()
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectRowAt(indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row + 1 == requestPage * 10 {
            requestPage = requestPage + 1
        }
    }
}
