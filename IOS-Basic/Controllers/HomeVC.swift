//
//  HomeVC.swift
//  IOS-Basic
//
//  Created by nikhil patil on 04/05/21.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet var homeTableview: UITableView!
    
    private var homeViewModel : HomeViewModel!
    
    private var dataSource : HomeTableViewDataSource<HomeTableCell, UserModel>!
    private var delegate : HomeTableViewDelegate<HomeTableCell, UserModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        callToViewModelForUIUpdate()
        
        // Do any additional setup after loading the view.
    }
    
    func callToViewModelForUIUpdate(){
        
        self.homeViewModel =  HomeViewModel()
        
        self.homeViewModel.bindEmployeeViewModelToController = {
            self.updateDataSource()
            self.updateDelegate()
        }
    }
    
    func updateDataSource(){
        self.dataSource = HomeTableViewDataSource(cellIdentifier: "HomeTableCell", items: self.homeViewModel.userData.data, configureCell: { (cell, evm) in            
            print("evm ---", evm.firstName)
            cell.configureCell(userData: evm)
        })
        
        DispatchQueue.main.async {
            self.homeTableview.dataSource = self.dataSource
            self.homeTableview.reloadData()
        }
    }
    
    func updateDelegate(){
        delegate = HomeTableViewDelegate(tableView: homeTableview, items: self.homeViewModel.userData.data)
        
        self.delegate.didSelectRowAt  = { (index) in
            print("didSelectTableView ---", index)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
