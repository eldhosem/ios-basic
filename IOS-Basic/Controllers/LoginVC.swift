//
//  LoginVC.swift
//  IOS-Basic
//
//  Created by nikhil patil on 28/04/21.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet var emailTextfield: UITextField!
    
    @IBOutlet var passwordTextfield: UITextField!
    
    var loginService = LoginService()
    
    // MARK: - Overrided functions
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func loginValidation() -> Bool{
        if emailTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.view.makeToast("Email field cannot be empty")
            return false
        } else if passwordTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
            self.view.makeToast("Password field cannot be empty")
            return false
        } else if AppUtils.validateEmail(enteredEmail: emailTextfield.text ?? ""){
            self.view.makeToast("Please enter valid email")
            return false
        }
        return true
    }
    
    // MARK: - Actions
    @IBAction func loginTapped(_ sender: Any) {
        if loginValidation() {            
            if !AppUtils.validateEmail(enteredEmail: emailTextfield.text ?? ""){
                let parameters = ["email": emailTextfield.text!, "password": passwordTextfield.text!, "user_type": "user", "device_type": "ios"]
                
           //     loginService.loginApi(parameters: parameters)
            } else{
                let parameters = ["email": emailTextfield.text!, "password": passwordTextfield.text!, "user_type": "user", "device_type": "ios"]
                
           //     loginService.loginApi(parameters: parameters)
                
            }
        }
        
        let nextVC = HomeVC.instantiateFromStoryboard(storyboardName: .Home, storyboardId: "HomeVC")
   //     self.navigationController?.pushViewController(nextVC, animated: true)
        self.present(nextVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
