//
//  Utils.swift
//  IOS-Basic
//
//  Created by nikhil patil on 29/04/21.
//

import Foundation
import SwiftyJSON

class AppUtils{
    /*
     To fetch the top most viewcontroller in current Window
     */
    static var topViewController: UIViewController?
    {
        var resultVC: UIViewController?
        resultVC = _topViewController(UIApplication.shared.keyWindow?.rootViewController)
        while ((resultVC?.presentedViewController) != nil) {
            resultVC = _topViewController(resultVC?.presentedViewController)
        }
        return resultVC
    }
    
    static func _topViewController(_ vc: UIViewController?) -> UIViewController? {
        if (vc is UINavigationController) {
            return _topViewController((vc as? UINavigationController)?.topViewController)
        } else if (vc is UITabBarController) {
            return _topViewController((vc as? UITabBarController)?.selectedViewController)
        } else {
            return vc
        }
        return nil
    }
    
    /*
     Developer:
     Use: To fetch user token saved in keychain.
     */
    class func fetchToken() -> String{
        if let retrievedToken = KeychainWrapper.standard.string(forKey: "token") {
            return retrievedToken
        }
        return ""
    }

    /*
     Use: To fetch user details
     */
    class func getUserDetails() -> UserModel?{
        if let UserDetails = UserDefaults.standard.value(forKey: "userDetails") as? [String:Any]{
            let userDetailsModel = UserModel(fromJson: JSON(UserDetails))
            return userDetailsModel
        }
        return nil
    }
    
    /*
     Use: To validate email format
     */
    class func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
}


@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = .black { didSet { updateColors() }}
    @IBInspectable var middleColor: UIColor = .white { didSet { updateColors() }}
    @IBInspectable var endColor: UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double = 0.05 { didSet { updateLocations() }}
    @IBInspectable var middleLocation: Double = 0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation: Double = 0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode: Bool = false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode: Bool = false { didSet { updatePoints() }}
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    var gradientLayer: CAGradientLayer {
        return (layer as? CAGradientLayer)!
    }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0): CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1): CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0): CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1): CGPoint(x: 0.5, y: 0)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, middleLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, middleColor.cgColor, endColor.cgColor]
        gradientLayer.type = .axial
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
        
    }
}
