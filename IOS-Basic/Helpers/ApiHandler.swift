
import Foundation
import Alamofire
import SwiftyJSON

class ApiHandler{
    /*
     Use: Function to call any Api.
     @params parameters
     @params viewToShow
     @params callUrl
     */
    
    public static var shared = ApiHandler()
    
    public static var apiRequestLog = [String: Int]()
    
    
    func callImageAPI(_ imageUrl: String, resultHandler: @escaping (_ data: Data?) -> ()) -> () {
        ApiHandler.sessionManager.request(imageUrl).response { response in
            if let imageData = response.data {
                if imageData != Data()
                {
                   resultHandler(imageData)
                }
                else  {
                   resultHandler(nil)
                }
            }
        }
    }
    
    
    public enum headerType {
        case ApplicationJson
        case FormUrlEncoded
    }
    
    public enum encodingMethodType {
        case JsonSerializationEncoding
        case UrlEncoding
    }
    
    
    static let sessionManager: Session = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = HTTPHeaders.default.dictionary
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 80
        
        let interceptor = Interceptor(
            retriers: [RetryPolicy(retryLimit: 3)]
        )
        
        let manager = Alamofire.Session(configuration: configuration, interceptor: interceptor)//Alamofire.Session(configuration: configuration)
        
      
        return manager
    }()

    
    struct ApiResponse{
        var response: JSON?
        var wasRequestFetched: Bool
        var errorMessage: String
    }

    
    public func callSomeAPI(_ parameters: Any, viewToShow: UIView? = nil, callUrl: String, defaultToken: String = AppUtils.fetchToken(), header: headerType =  headerType.FormUrlEncoded, method: HTTPMethod = .post, encodingMethod: encodingMethodType = .UrlEncoding,  showLoader: Bool = true, resultHandler: @escaping (_ data: JSON?, _ wasRequestFetched: Bool, _ errorMessage: String) -> ()) -> () {
        //let sessionConfig = Session(interceptor: interceptor)
        //sessionConfig.sessionConfiguration
        
        
        print("api-called")
        print("ReachabilityManager", ReachabilityManager.isConnectedToNetwork())
        if !ReachabilityManager.isConnectedToNetwork(){
            if let topVC = AppUtils.topViewController{
                topVC.popupAlert(title: "Internet Unavailable", message: "Your device has lost it's internet connection", actionTitles: ["Retry", "Try Later"], actions:[{action1 in
                    let retryParam = parameters
                    let retryViewToShow = viewToShow
                    let retryCallUrl = callUrl
                    let retryResultHandler = resultHandler
                    self.callSomeAPI(retryParam, viewToShow: retryViewToShow, callUrl: retryCallUrl, resultHandler: retryResultHandler)
                    }, {action2 in }, nil])
                resultHandler(JSON.null, true, "Your device has lost it's internet connection")
            }
        }
        
        
        
        
        var headers: HTTPHeaders = ["Content-Type": "application/json", "token": defaultToken, "origin": "itEze"]
        if header == headerType.FormUrlEncoded{
            headers = ["Content-Type": "application/x-www-form-urlencoded", "token": defaultToken, "origin": "itEze"]
        }
        
        
        print("callUrl",callUrl)
        print("parameters \(parameters) \n headers  \(headers)")
        print("Method \(method)")
        
        var tempParameter: [String:Any]? = parameters as? Dictionary<String, Any>
        var tempEncodingMethod: ParameterEncoding = URLEncoding.httpBody
        if encodingMethod == .JsonSerializationEncoding{
            tempParameter = [:]
            let jsonParamters = try! JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            let jsonString = String(data: jsonParamters, encoding: .utf8)!
            tempEncodingMethod = BodyStringEncoding(body: jsonString)
        }
        
        
        if showLoader{
            print("Spinner started by callUrl: ",callUrl)
            Spinner.start()
        }
        print("AppUtils.apiRequestLog[callUrl]", ApiHandler.apiRequestLog[callUrl] as Any)
        ApiHandler.sessionManager.request(callUrl, method: method, parameters: tempParameter, encoding: tempEncodingMethod, headers: headers).responseJSON { response in
            
            
            print("response statusCode for \(callUrl): \(response.response?.statusCode ?? 0)")
            
           
            print("response for \(callUrl): \n", response)
            
            
            
            switch response.result
            {
                
            case .success(let res):
                let userData = JSON(res)
                print("userData",userData )
                let currentWindow: UIWindow? = UIApplication.shared.keyWindow
                
                if userData["msg"].stringValue == "user_deleted" || userData["msg"].stringValue == "user_blocked" || userData["msg"].stringValue == "user_disabled" || userData["msg"].stringValue == "user_not_found"{
                    if let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window {
                        if let appDomain = Bundle.main.bundleIdentifier {
                            UserDefaults.standard.removePersistentDomain(forName: appDomain)
                            let initialViewController = LoginVC.instantiateFromStoryboard(storyboardName: .Login, storyboardId: "LoginVC")
                            window.rootViewController = initialViewController
                        }
                        currentWindow?.makeToast(userData["title"].stringValue)
                    }else if let window = UIApplication.shared.windows.first {
                        
                        print("app.window- nil", window)
                        if let appDomain = Bundle.main.bundleIdentifier {
                            UserDefaults.standard.removePersistentDomain(forName: appDomain)
                            let initialViewController = LoginVC.instantiateFromStoryboard(storyboardName: .Login, storyboardId: "LoginVC")
                            window.rootViewController = initialViewController
                        }
                        currentWindow?.makeToast(userData["title"].stringValue)
                    }else{
                        
                        currentWindow?.makeToast("Something went wrong. :( \n Kindly close the app and reopen it.")
                    }
                }
                
                resultHandler(userData, true, userData["title"].stringValue)
                if showLoader{
                    print("Spinner stopped by callUrl: ",callUrl)
                }
                
                if showLoader{
                    Spinner.stop()
                }
                break
                
            case .failure( _):
                resultHandler(JSON.null, false,  Config.defaultErrorMessage)
                
                if showLoader{
                    Spinner.stop()
                }
                
                print("response.error.localizedDescription", response.error?.localizedDescription.description as Any)
                print("response.error.statusCode", response.response?.statusCode as Any)
                
                if response.error?.localizedDescription == "URLSessionTask failed with error: cancelled"{
                    return
                }
                
                
                if response.response?.statusCode == 503 || response.response?.statusCode == 404{
                    return
                }
                
                var alertTitle = "Error"
                var alertMessage = "Something went wrong :("
                if !ReachabilityManager.isConnectedToNetwork(){
                    alertTitle = "Internet Unavailable"
                    alertMessage = "Your device has lost it's internet connection"
                }
            }
        }
    }
    
    
    
    static func decode<T: Decodable>(_ type: T.Type, from jsonObj: JSON) -> T? {
        if let strJson = jsonObj.rawString() {
            if let data = strJson.data(using: .utf8) {
                guard let modelObj = try? JSONDecoder().decode(T.self, from: data) else { return nil}
                return modelObj
            }
        }
        return nil
    }
    
}





struct BodyStringEncoding: ParameterEncoding {
    
    private let body: String
    
    init(body: String) { self.body = body }
    
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        guard var urlRequest = urlRequest.urlRequest else { throw Errors.emptyURLRequest }
        guard let data = body.data(using: .utf8) else { throw Errors.encodingProblem }
        urlRequest.httpBody = data
        return urlRequest
    }
}

extension BodyStringEncoding {
    enum Errors: Error {
        case emptyURLRequest
        case encodingProblem
    }
}

extension BodyStringEncoding.Errors: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .emptyURLRequest: return "Empty url request"
        case .encodingProblem: return "Encoding problem"
        }
    }
}

