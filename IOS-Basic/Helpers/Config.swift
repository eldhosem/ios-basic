//
//  Config.swift
//  IOS-Basic
//
//  Created by nikhil patil on 04/05/21.
//

import Foundation

public enum AppStoryboard : String {
    case Login
    case Home
}

class Config {
    static var baseUrl = "https://astrowize.infiny.dev/"
    
    static var loginApi = "user/login"
    
    static var homeApi = "user/login"
    
    static var getAstrologerList = "admin/getAstrologerListApp"
    
    public static var defaultErrorMessage = "Oops! Something went wrong. Please try again"
}
