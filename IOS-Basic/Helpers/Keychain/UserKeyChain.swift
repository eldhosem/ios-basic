//
//  UserKeyChain.swift
//  MediDeals
//
//  Created by Apple on 2/14/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserKeyChain {
    static func getKeyChainData(key: String) -> JSON? {

            if let retrievedUser = KeychainWrapper.standard.string(forKey: key) {

                print("Retrieved retrievedUser is: \(retrievedUser)")

                let userData = JSON.init(parseJSON: retrievedUser)

                

                return userData

            }

            

            return nil

            

        }





    static func getUserData() -> JSON? {

            if let retrievedUser = KeychainWrapper.standard.string(forKey: "UserDetails") {

                print("Retrieved retrievedUser is: \(retrievedUser)")

                let userData = JSON.init(parseJSON: retrievedUser)

                

                return userData

            }

            

            return nil

            

        }





    static func removeNullFromDict(dictionaryData: [String: Any]) {

            var dict = dictionaryData

            /*

             for (key, value) in dict {

             if dict[key] is NSNull {

             dict[key] = ""

             }

             

             var arr = dict[key] as? [[String: Any]]

             if arr != nil {

             for (i, data) in arr!.enumerated() {

             for (key1, value1) in data {

             if (data[key1] is NSNull) {

             arr![i][key1] = ""

             }

             }

             }

             dict[key] = arr

             }

             }

             UserDefaults.standard.setValue(dict, forKey: "userDetails")

             */

            //     var dict = ["name": "", "user_type": "", "user_status": "", "": ""]

            do {

                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])

                let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)!

                print ("removeNullFromDict jsonString \(jsonString)")

                

                let saveSuccessful: Bool = KeychainWrapper.standard.set(jsonString, forKey: "UserDetails")

                

                print("removeNullFromDict saveSuccessful \(saveSuccessful)")

            } catch {

                print("removeNullFromDict error \(error.localizedDescription)")

            }

        }





    static func removeTokenFromKeyChain(key: String) {

            let removeSuccessful: Bool = KeychainWrapper.standard.remove(key: key)

            

            if key == "token" {

                UserDefaults.standard.removeObject(forKey: "ShowOnbaording")

            }

            print("saveSuccessful  \(removeSuccessful)")

        }
}
