
//
//  Spinner.swift
//  ItsEZE
//
//  Created by Apple on 12/18/19.
//  Copyright © 2019 Apple. All rights reserved.
//
//  

import UIKit

open class Spinner {
    internal static var spinnerWrapper: UIView?
    internal static var spinner: UIActivityIndicatorView?
    public static var style: UIActivityIndicatorView.Style = .large
    public static var baseBackColor = UIColor.black.withAlphaComponent(0.5)
    public static var baseColor = UIColor.green
    
    public static func start(style: UIActivityIndicatorView.Style = style, backColor: UIColor = baseBackColor, baseColor: UIColor = baseColor) {
       // NotificationCenter.default.addObserver(self, selector: #selector(update), name: NSNotification.Name.UIDevice.orientationDidChangeNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: UIDevice.orientationDidChangeNotification, object: nil)
        if spinner == nil, let window = UIApplication.shared.keyWindow {
            let frame = UIScreen.main.bounds
            spinner = UIActivityIndicatorView(frame: frame)
            spinner!.backgroundColor = .clear//backColor
            spinner!.style = style
            spinner?.color = baseColor
            
            spinnerWrapper = UIView()
            spinnerWrapper?.frame  = frame
            
            let blurView = UIView(frame: frame)
            blurView.alpha = 1
            blurView.backgroundColor = .clear
            
            spinnerWrapper?.addSubview(blurView)
            spinnerWrapper?.addSubview(spinner!)
            
            window.addSubview(spinnerWrapper!)
            spinner!.startAnimating()
        }
    }
    
    public static func stop() {
        if spinner != nil {
            spinner!.stopAnimating()
            spinner!.removeFromSuperview()
            spinner = nil
            spinnerWrapper!.removeFromSuperview()
            spinnerWrapper = nil
        }
    }
    
    @objc public static func update() {
        if spinner != nil {
            stop()
            start()
        }
    }
}
